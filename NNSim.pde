int xPos = 0;
int yPos = 0;
int xDimMatrix = 4;
int yDimMatrix = 3;
int xDist = 200;
int yDist = 80;

Node[][] nodeMatrix;
Node node;
NodeList nodeList;

Conn[][][] connMatrix;
ConnList connList;
int index1 = 0, index2 = 0;

void setup() {
  size(900,500);
  nodeMatrix = new Node[xDimMatrix][yDimMatrix];
  nodeList = new NodeList();
  connMatrix = new Conn[xDimMatrix - 1][yDimMatrix][yDimMatrix];
  connList = new ConnList();
  
  nodeList.createNodeMatrix(); 
  connList.createConnMatrix();
}

void draw(){
  background(230);
  nodeList.displayNodes();   
  connList.displayAllConn();
  nodeList.displayNodeStructure();  
  noLoop();  
}
