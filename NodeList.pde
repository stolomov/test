class NodeList {
  
void displayNodeStructure(){ 
  fill(0);
  text("Spalten: " + nodeMatrix.length, 50, 10);
  text("Zeilen: " + nodeMatrix[0].length, 50, 20); 
}

void createNodeMatrix(){
  for (int i=0; i < xDimMatrix; i++){
    xPos = xPos + xDist;
    yPos = 0;
      for(int j = 0; j < yDimMatrix; j++){
        yPos = yPos + yDist;
        node = new Node();
        node.setXPos(xPos);
        node.setYPos(yPos);
        nodeMatrix[i][j]= node;
    }
  }
}

void displayNodes(){
    for(int i = 0; i < nodeMatrix.length; i++){
      for (int j = 0; j < nodeMatrix[i].length; j++){
        Node currentNode = nodeMatrix[i][j];
        currentNode.displayNode();
        currentNode.nodeValue();     
      }
    }
  }
}
